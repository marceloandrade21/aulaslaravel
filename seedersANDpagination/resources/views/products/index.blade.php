<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  
<div class="container text-center">
    @forelse($products as $product)
        <p>{{ $product->name }}</p>
    @empty
        Não existem produtos
    @endforelse

    {{$products->links()}}



    <br style="padding:30px;">
    <select name="product">
           @forelse($products as $product)
                <option value="{{$product->id}}">
                {{ $product->name }}
                </option>
            @empty
                Não existem produtos
            @endforelse

    </select>
</div>

</body>
</html>