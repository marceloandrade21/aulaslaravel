<?php

use Illuminate\Database\Seeder;
use App\Product;
class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<100;$i++)
        {
        $product = new Product();
        $product->name = "Produto $i";
        $product->description = "Produto $i";
        $product->price = "1";
        $product->save();
        }
    }
}
