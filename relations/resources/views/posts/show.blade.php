<h1>{{$post->title}}</h1>
<ul>
    @foreach($post->tags as $tag)
        <li> {{ $tag->text}}</li>
    @endforeach
</ul>
<div>
{{ $post->body}}
</div>
@foreach($post->comments as $comment)
<div>
{{$comment->text}}
</div>
@endforeach