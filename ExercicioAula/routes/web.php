<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/messages','MessagesController@home');

Route::resource('/courses', 'CourseController');
Route::resource('/candidates', 'CandidatesController');
Route::resource('/sendmessage', 'MessagesController');
Route::get('/admin','PagesController@admin');
Route::get('/admin/courses','PagesController@addCourse');
