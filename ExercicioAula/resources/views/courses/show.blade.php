@extends('layouts.base')

@section('title')
{{ $course->id }} - {{ $course->name }}
@endsection

@section('content')
<p>
    <b>Curso</b>
    {{ $course->name }}
</p>
<p>
    <b>Data</b>
    {{ $course->date }}
</p>
<p>
    <b>Certificações</b>
    {{ $course->certification }}
</p>
<p>
    <b>Conteúdo da Formação</b>
    {{ $course->content }}
</p>
<label>Certificações</label>
<ul>
@foreach($course->certifications as $certification)
    <li>{{$certification->name}}</li>
@endforeach
</ul>

<p>
<h1> Candidata-te já !</h1>
<form method="post" action="/candidates">
    {{ csrf_field() }}
    Curso: <input type="text" name="course_id" value="{{$course->id}}" readonly><br>
    Nome:<input type="text" name="name" placeholder="Nome"> <br>
    Email:<input type="text" name="email" placeholder="Email@Email.pt"> <br>
    Telemóvel:<input type="text" name="phone" placeholder="Telemovel"> <br>
    Descrição:<br><textarea rows="4" cols="50" name="description"> </textarea><br>
    Data Nascimento:<input type="text" id="datepicker" name="datenasc" placeholder="Selecione a data"> <br>
    <input type="submit" value="Enviar">
</form>
</p>

@endsection