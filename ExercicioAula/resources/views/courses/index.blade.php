@extends('layouts.base')

@section('title')
Cursos
@endsection

@section('content')

<table border="1" style="width:100%; text-align:center;" class=" table table-striped table-hover">
    <thead >
        <tr>
            <td> Nome Curso</td>
            <td> Sabe Mais </td>
        </tr>
    </thead>
    <tbody>
        @foreach($courses as $course)
        <tr>
        <td>{{ $course->name }}</td>
        <td><a href="/courses/{{ $course->id }}">Detalhes</a></td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection('content')