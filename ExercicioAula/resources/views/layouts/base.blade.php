<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
    <style>
    body, html {
        text-align:center;
    }
    </style>

    <title>
        @yield('title')
    </title>
</head>
<body>

            <nav class="navbar navbar-default">
            <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"><img src="https://www.portugalespanha.org/images/Logotipos/ATEC-Logo.png" style="width:60px"/></a>
            </div>
            
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">A atec</a></li>
                <li><a href="/courses">Cursos</a></li>
                <li><a href="/messages">Contacta-nos</a></li>
                <li><a href="/admin" data-toggle="dropdown">Administração</a></li>
                
            </ul>
            </div>
            </nav>

   

    <div class="content">
        <h1> @yield('title')</h1>
        @yield('content')
    </div>

    <div class="container text-center footer navbar-fixed-bottom">

        <hr>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-pills nav-justified">
                        <li>© 2017 atec</li>
                        <li>Marcelo Andrade Design</li>
                        <li>Laravel</li>
                    </ul>
                </div>
            </div>
    </div>
</body>
</html>