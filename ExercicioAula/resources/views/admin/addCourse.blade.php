@extends('layouts.base')

@section('title')
Gerir Cursos
@endsection

@section('content')
<table border="1" style="width:100%; text-align:center;" class=" table table-striped table-hover">
    <thead >
        <tr>
            <td> Curso</td>
            <td> Data </td>
            <td> Conteúdo da Formação </td>
           
        </tr>
    </thead>
    <tbody>
        @foreach($courses as $Course)
        <tr>
        <td>{{ $Course->name }}</td>
        <td>{{ $Course->date }}</td>
        <td>{{ $Course->content }}</td>
       
        </tr>
        @endforeach
    </tbody>
</table>
@endsection('content')