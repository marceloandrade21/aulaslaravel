@extends('layouts.base')

@section('title')
Administração
<br><hr><br>
Mensagens
@endsection

@section('content')

<a href="/admin/courses">Gerir Cursos</a>
<table border="1" style="width:100%; text-align:center;" class=" table table-striped table-hover">
    <thead >
        <tr>
            <td> Nome</td>
            <td> Email </td>
            <td> Assunto </td>
            <td> Mensagem </td>
            <td> Opções </td>
        </tr>
    </thead>
    <tbody>
        @foreach($Messages as $Message)
        <tr>
        <td>{{ $Message->name }}</td>
        <td>{{ $Message->email }}</td>
        <td>{{ $Message->subject }}</td>
        <td>{{ $Message->message }}</td>
        <td> </td>
        </tr>
        @endforeach
    </tbody>
</table>
<hr><br>
<h1> Candidaturas </h1>
<table border="1" style="width:100%; text-align:center;" class=" table table-striped table-hover">
    <thead >
        <tr>
            <td> Curso</td>
            <td> Nome </td>
            <td> Email </td>
            <td> Telenóvel </td>
            <td> Descrição </td>
            <td> Data Nascimento </td>
            <td> Opções </td>

        </tr>
    </thead>
    <tbody>
        @foreach($Candidates as $Candidate)
        <tr>
        <td>{{ $Candidate->course_id }}</td>
        <td>{{ $Candidate->name }}</td>
        <td>{{ $Candidate->email }}</td>
        <td>{{ $Candidate->phone }}</td>
        <td>{{ $Candidate->description }}</td>
        <td>{{ $Candidate->datenasc }}</td>
        <td> <form method="post" action="/candidates/{{ $Candidate->id}}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" value="Eliminar">
              </form> 
        </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection