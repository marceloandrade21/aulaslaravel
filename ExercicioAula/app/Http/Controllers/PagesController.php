<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Candidate;
use App\Course;

class PagesController extends Controller
{
    public function home()
    {
        return view('courses.presentation');
    }
    public function admin()
    {
        $Messages = Message::all();
        $Candidates = Candidate::all();
        return view('admin.index')->with(compact('Messages','Candidates'));
    }
    public function addCourse(){

        $courses = Course::all();
        return view('admin.addCourse')->with(compact('courses'));
        
    }
}
