<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function certifications()
    {
        return $this->belongsToMany(Certification::class);
    } 
}
