function soma(){
    var n1 = parseInt(document.querySelector("#num1").value);
    var n2 = parseInt(document.querySelector("#num2").value);
    var p = document.querySelector("#resultado");
    var final = n1+n2;
    p.innerHTML = "<b>Resutaldo</b> = " + final;
  
};

function copia(){
    var original = document.querySelector("#original");
    var copia = document.querySelector("#copia");
    copia.value = original.value;
};

function paradiv(){
    var original = document.querySelector("#caixatexto");
    var div = document.querySelector("#div");
    div.innerHTML = original.value;
};

function texto5xdiv(){
    var original = document.querySelector("#texto5x");
    var div = document.querySelector("#div5x");
    for(var i=1;i<6;i++){
        div.innerHTML += original.value +"<br>";
    }
};
function separa(){
    var original = document.querySelector("#texto");
    var virgulas = document.querySelector("#virgulas");

    virgulas.value = original.value.split(" ");
    original.value = "";
};

function lista(){
    var original = document.querySelector("#caixalista");
    var lista = document.querySelector("#lista");
    var palavras = original.value.split(" ");
    
    for(var i=0; i<palavras.length;i++){
        var li = document.createElement("li");
        li.innerHTML = palavras[i];
        lista.appendChild(li);
    }

};

function listaimpares(){
    var original = document.querySelector("#caixalistaul");
    var lista = document.querySelector("#listaul");
    var numeros = original.value.split(" ");

    for(var i=0; i<numeros.length;i++){
        var li = document.createElement("li");
        if(numeros[i]%2==0){
            li.innerHTML = numeros[i];
        }else {
            li.innerHTML = numeros[i] + " É IMPAR ";
        }
        lista.appendChild(li);
    }
};
