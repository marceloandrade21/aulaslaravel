
Vue.config.webtools=true; // MODO DE DESENVOLVIEMNTO PARA USAR O VUE DEVTOOLS
// VAMOS DEFINIR A ONDE A VUE JS TRABALHA
new Vue({
    el: "#app", // É O ELEMENTO COM O ID APP
    data: {
        message: "Hello World!"
    }
});
// LISTAR UM ARRAY 
new Vue({
    el: "#app1", 
    data: {
        items: ["PS4","Xbox 1","Wii"]
    }
});


// RECEBER E LISTAR UM ARRAY DINÂMICO ATRAVÉS AO QUE O USER INTRODUZIR NA CAIXA DE TEXTO 
new Vue({
    el: "#app2", 
    data: {
        newItem: "",
        items: ["PS4","Xbox 1","Wii"]
    },
    methods: {
        addItem(){
            this.items.push(this.newItem);
            this.newItem ="";
        }
    }

});

// TAREFAS 
new Vue({
    el: "#app3", 
    data: {
        newTask: "",
        tasks: [
             {
                name: "Codificar em Laravel",
                completed:true
        },
        {
            name: "Codificar em VueJS",
            completed:false
        }]
    },
    methods: {
        addTask(){
            this.tasks.push({
                name: this.newTask,
                completed: true
            }
        );
            this.newTask ="";
        }
    }

});