import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import ContactsIndex from '@/components/ContactsIndex'
import ContactsShow from '@/components/ContactsShow'
import ContactsCreate from '@/components/ContactsCreate'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Home
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: ContactsIndex
    },
    {
      path: '/contacts/create',
      name: 'Create Contact',
      component: ContactsCreate
    },
    {
      path: '/contacts/:id',
      name: 'Contact',
      component: ContactsShow
    }
  ]
})
