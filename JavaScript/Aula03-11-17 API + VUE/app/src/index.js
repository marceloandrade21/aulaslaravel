import Vue from 'vue'; // vai usar o vue js
import axios from 'axios';
new Vue({
    el: "#app",
    data:{
        tasks:[]
    },

    methods:{

    },
    mounted(){ // no momento que a página é carregada
        axios.get('http://localhost:8000/tasks') // pedido ao endereço 
            .then(response=>{
                this.tasks=response.data;
            })
    }
});