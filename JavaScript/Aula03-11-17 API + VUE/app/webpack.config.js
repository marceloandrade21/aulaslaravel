module.exports = {
    entry: ['./src/index.js'],
    output: {
        path: __dirname + '/dist',
        filename: 'index.js'
    },
    module: {
        loaders: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }]
    },
    devServer: {
        port: 3000,
        contentBase: './dist',
        inline: true
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
        }
    },
}