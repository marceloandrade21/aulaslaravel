var pessoa1 = {
    name: "Marcelo",
    age: 19, 
    address: "Matosinhos",
}

var pessoa2 = pessoa1; // isto cria um apontador para a pessoa1 e não cria um objeto novo.


/*******************************************
 *                                         * 
 *                                         *
 *          INSTÂNCIAR OBJETOS             *
 *                                         *
 *                                         *
/*******************************************
 *  FACTORY PARA CRIAÇÃO DE VÁRIOS OBJETOS *
 *  EXEMPLO CRIAR VÁRIOS OBJETOS USERS     *
 * ****************************************/
// para criar um objeto que cria objetos usamos um "Factory" - (Design Parterns)
var createUser = function(username,name,email){
    var user = {};
        user.username = username;
        user.name = name;
        user.email = email;
    
    return user;
}

var user1 = createUser('user1','User 1','');
var user2 = createUser('user2','User 2','');

            /******************* OU **********************
/* DEFINE O OBJETO PORÉM PODE SER MANIPULADO PELO EXTERIOR, CONVÉM PROTEGER */
var User = function(username,name,email){ 
    this.username = username;
    this.name = name;
    this.email = email;
}
var user3 = new User('user1','User 1','');
var user4 = new User('user2','User 2','');
//Object.preventExtensions(user4); isto protege o objeto porém não é garantido que funcione em todos os Browsers

/******** DEFINIR UM PROTOTYPE PARA PROTEÇÃO DO OBJETO *********
Todos os Objetos tem um Prototype 
 O prototype é as regras para todos os objetos que vão ser criados apartir deste */
var parent = function(){ }
parent.prototype.age=10;
parent.prototype.name ="Marcelo";
var child1 = new parent();
var child2 = new parent();





