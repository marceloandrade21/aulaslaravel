import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import CarsIndex from '@/components/CarsIndex'
import CarsCreate from '@/components/CarsCreate'
import CarsShow from '@/components/CarsShow'
import CarsEdit from '@/components/CarsEdit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/cars',
      name: 'CarsIndex',
      component: CarsIndex
    },
    {
      path: '/cars/new',
      name: 'CarsCreate',
      component: CarsCreate
    },
    {
      path: '/cars/:id',
      name: 'CarsShow',
      component: CarsShow
    },
    {
      path: '/cars/:id/update',
      name: 'CarsEdit',
      component: CarsEdit
    }
  ]
})
