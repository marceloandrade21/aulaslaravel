/*

colocar o <script src=".js"></script> no final do html
um objeto é uma função
<> </> identifica o início e o fim do 

< tag="text/java" src="app.js"> </> ( vai buscar o ficheiro com as suas funções )

**************************************
** IDENTIFICAR ELEMENTOS**
**************************************
document.getElementByID("id-name"); // BY ID 
document.getElementsByTagName("tag-name"); // BY TAG NAME
document.querySelector("query"); // ATRAVÉS DOS SELECTORES DE CSS ( RETORNA 1 )

******************
** VARIÁVEIS **
******************
Não existe tipo de variáveis
Existem porém ( string, number, boolean, array, object )
Identificar o tipo : typeof variable_name

undefined ->  quando uma variável é declarada mas nunca lhe foi atribuido um valor
null -> é um valor

=== -> compara o valor e o tipo

ARRAYS:
Declaração :  var ages = [13,31,23,19];
Tamanho: ages.length
Adicionar(no fim do array) : ages.push(36);
Remover(o último) : ages.pop();
Aceder a uma posição : ages[pos];
Obter a posição de determinado valor : ages.indexOf(23);
Extrair determinado(s) valor(es) : ages.splice(pos,length);

****************
** CICLO FOR ***
****************
for(var i=1; i<= 10; i++){
    // instruções
}

***************
*** FUNÇÕES ***
***************
Declaração: 
function print(text){
    console.log(text);
}
Chamada:
print("Olá Mundo");

*************
** OBJETOS ** COLEÇÃO DE DADOS
*************
Declaração:
var person = {
    name: 'John',
    age: 13,
    nationality: "Portuguese",
};
Aceder a um atributo:
person.name (propriedade) ou person["name"] (array)

document.createElement("p"); // CRIA O ELEMENTO P
div.appendChild(p); // ADICIONA O ELEMENTO P AO ELEMENTO DIV
*/

document.write("Olá Mundo"); // ESCREVE NA PÁGINA HTML


// VERIFICA SE UM NÚMERO É IMPAR OU PAR
document.write("<p><b>EXERCÍCIO PAR OU IMPAR</b></p>");
var num = 2;

if(num % 2 == 0){
    document.write("<br>O número " + num +" é Par");
}
else {
    document.write("<br>O número " + num +" é Impar");
}

// TERNÁRIO QUE VERIFICA SER PAR OU IMPAR
document.write(num%2==0?"<br>Par":"<br>Impar");
 


// DADO UM ARRAY COM NOMES DE PESSOAS APRESENTAR OS NOMES DE TODAS AS PESSOAS;
document.write("<p><b>EXERCÍCIO NOMES EM ARRAY</b></p>");
var nomes = ['Marcelo','André','João','Ricardo','Nuno','Paulo','António'];

for(var i=0; i<nomes.length;i++){
    document.write("<br> " + nomes[i] + "." );
}

// ALTERAR O CONTEÚDO DE UM ELEMENTO
var title = document.querySelector("#title");
title.innerHTML="Olá Mundo";

// ADICIONAR UM PARÁGRAFO DENTRO DE UMA DIV
var p = document.createElement("p");
p.innerHTML = "Adicionado o Parágrafo à div.";
var div = document.querySelector("#content");
div.appendChild(p);