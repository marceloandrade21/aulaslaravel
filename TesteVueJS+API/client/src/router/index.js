import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import TasksIndex from '@/components/TasksIndex'
import TasksCreate from '@/components/TasksCreate'
import TasksUpdate from '@/components/TasksUpdate'
import TasksArchive from '@/components/TasksArchive'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: HelloWorld
    },
    {
      path: '/tasks',
      name: 'TasksIndex',
      component: TasksIndex
    },
    {
      path: '/tasks/new',
      name: 'TasksCreate',
      component: TasksCreate
    },
    {
      path: '/tasks/:id/update',
      name: 'TasksUpdate',
      component: TasksUpdate
    },
    {
      path: '/tasks/archive',
      name: 'TasksArchive',
      component: TasksArchive
    }
  ]
})
