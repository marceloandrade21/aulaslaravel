@extends('layouts.app')


@section('content')
<div class="container">

<form method="get" action="/product/search">
    <input type="text" name="search"/>
    <input type="submit" value="pesquisar"/>
</form>
    <table class="table">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Preço</th>
            <th>Ações</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($products as $product)
            <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->price }}</td>
            <td><a href="/product/{{ $product->id }}">Detalhes</a></td>
            @if(Auth::user()->is_admin == 1)
              <td><form method="post" action="/product/{{ $product->id}}">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <input type="submit" class="btn btn-info" value="Eliminar">
                      </form> </td>
            @endif
            @endforeach
            </tr>
        </tbody>
      </table>

 <ul class="text-center list-group">

    @if(Auth::user()->is_admin == 1)
            <li class="list-group-item"><a href="/product/create">Adicionar Produto</a> </li>
    @endif 

</ul>
</div>
@endsection