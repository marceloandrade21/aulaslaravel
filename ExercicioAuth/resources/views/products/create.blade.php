@extends('layouts.app')


@section('content')
<div class="container">
        <form method="post" action="/product">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="text">Nome:</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="text">Descrição:</label>
            <input type="text" class="form-control" name="description">
        </div>
        <div class="form-group">
            <label for="text">Preço:</label>
            <input type="text" class="form-control" name="price">
        </div>
        
        <button type="submit" class="btn btn-default">Adicionar</button>
        </form>
</div>
@endsection