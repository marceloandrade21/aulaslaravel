@extends('layouts.app')


@section('content')
<div class="container">
        <form method="post" action="/product/{{ $product->id }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="form-group">
            <label for="text">Nome:</label>
            <input type="text" class="form-control" value=" {{ $product->name }}" name="name">
        </div>
        <div class="form-group">
            <label for="text">Descrição:</label>
            <input type="text" class="form-control" value=" {{ $product->description }}" name="description">
        </div>
        <div class="form-group">
            <label for="text">Preço:</label>
            <input type="text" class="form-control" value=" {{ $product->price }}" name="price">
        </div>
        
        <button type="submit" class="btn btn-default">Atualizar</button>
        </form>
</div>
@endsection