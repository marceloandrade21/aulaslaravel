@extends('layouts.app')


@section('content')

<div class="container">
    <ul class="text-center list-group">

        <li class="list-group-item"><b>Nome:</b> {{ $product->name}}</li>

        <li class="list-group-item"><b>Descrição:</b> {{ $product->description}}</li>

         <li class="list-group-item"><b>Preço:</b> {{ $product->price}}</li>

          @if(Auth::user()->is_admin == 1)
            <li class="list-group-item"><a href="/product/{{ $product->id }}/edit">Editar Produto</a> </li>
            
          @endif 
       
    </ul>
</div>
   
   
@endsection