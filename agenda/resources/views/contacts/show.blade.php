@extends('layouts.base')

@section('title')
{{ $contact->id }} - {{ $contact->name }}
@endsection

@section('content')
<p>
    <b>Nome</b>
    {{ $contact->name }}
</p>
<p>
    <b>Email</b>
    {{ $contact->email }}
</p>
<p>
    <b>Telefone</b>
    {{ $contact->phone }}
</p>
@endsection