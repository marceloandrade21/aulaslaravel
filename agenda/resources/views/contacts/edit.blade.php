@extends('layouts.base')

@section('title')
Editar Contacto
@endsection

@section('content')
<form method="post" action="/contacts/{{ $contact->id }}">
    {{ method_field('PUT') }}
    @include('contacts.form')
    <p>
        <input type="submit" value="Alterar">
    </p>
</form>
@endsection('content')