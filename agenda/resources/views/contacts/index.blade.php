@extends('layouts.base')

@section('title')
Contactos
@endsection

@section('content')
<p>
    <a href="/contacts/create">Novo</a>
</p>
<table>
    <thead>
        <tr>
            <td>Nome</td>
            <td>Email</td>
            <td>Telefone</td>
            <td>Ações</td>
        </tr>
    </thead>
    <tbody>
        @foreach($contacts as $contact)
        <tr>
            <td>{{ $contact->name }}</td>
            <td>{{ $contact->email }}</td>
            <td>{{ $contact->phone }}</td>
            <td>
                <a href="/contacts/{{ $contact->id }}">Detalhes</a>
                <a href="/contacts/{{ $contact->id }}/edit">Editar</a>
                <form method="post" action="/contacts/{{ $contact->id}}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" value="Eliminar">
                </form> 
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection('content')