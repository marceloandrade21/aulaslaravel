@extends('layouts.base') 

@section('title') 
Novo Contacto 
@endsection 

@section('content')

<form method="post" action="/contacts">
    @include('contacts.form')

    <p>
        <input type="submit" value="Criar">
    </p>
</form>

@endsection
