@if($errors->any)
    <div style="color:red">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif

{{ csrf_field() }}
<p>
    <label>Nome</label>
    <input type="text" name="name" value="{{ old('name')?:$contact->name }}">
</p>
<p>
    <label>Email</label>
    <input type="email" name="email" value="{{ old('email')?:$contact->email }}">
</p>
<p>
    <label>Telefone</label>
    <input type="text" name="phone" value="{{ old('phone')?:$contact->phone }}">
</p>