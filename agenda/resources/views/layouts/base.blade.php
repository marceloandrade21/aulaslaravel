<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <style>
    body, html {
        margin: 0;
    }
    </style>

    <title>
        @yield('title')
    </title>
</head>
<body>

    <div style="height: 100px; background-color: #CCC;">
    </div>

    <div style="float:left; width:100px; min-height:300px; background:#ddd;">
    </div>

    <div class="content">
        <h1> @yield('title')</h1>
        @yield('content')
    </div>

    <div style="clear:both; height:100px; background:#CCC"> 
    </div>

</body>
</html>